﻿using System;
using System.Web.Mvc;
using FauxSuperheroSprint.UI.Models;
using System.Net.Mail;
using System.Net;
namespace FauxSuperheroSprint.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel contact)
        {
            //Create the body for the E-Mail
            string body = string.Format($"Name: {contact.Name}<br>" + $"Email: {contact.Email}<br>Subject: {contact.Subject} " +
                $"Message:<br>{contact.Message}");
            //Create and configure the Mail Message Object
            MailMessage msg = new MailMessage("no-reply@thecodegym.com", //From address on hosting account, 
                "nathangeranis@outlook.com", //To Address where teh email will be delivered,
                contact.Subject + "-" + DateTime.Now, body);
            //Extra Configuration
            msg.Priority = MailPriority.High;
            msg.IsBodyHtml = true;
            msg.CC.Add("nathangeranis@outlook.com");

            //Create and configure the SMTP Client
            SmtpClient client = new SmtpClient("mail.thecodegym.com");
            client.Credentials = new NetworkCredential("no-reply@thecodegym.com", "P@ssw0rd!23");
            //Attempt to send the email
            using (client)
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        client.Send(msg);
                    }
                    else
                    {
                        return View();
                    }
                }
                catch
                {
                    ViewBag.ErrorMessage = "There was a problem. Please try again.";
                    return View();
                }
            }

            return View("ContactConfirmation", contact);
        }

    }
}
