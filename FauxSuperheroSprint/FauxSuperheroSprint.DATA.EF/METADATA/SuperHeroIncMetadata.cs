﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace FauxSuperheroSprint.DATA.EF.METADATA
{
    public class SuperHeroIncMetadata
    {
    }
    public class CourseMetadata
    {
        [Display(Name = "Course")]
        [StringLength(50, ErrorMessage = "*Less than 50 ")]
        [Required(ErrorMessage = "*Required")]
        public string Name { get; set; }

        [Display(Name = "Description")]
        [UIHint("MultilineText")]
        [DisplayFormat(NullDisplayText = "N/A")]
        public string Description { get; set; }
    }//CourseMetadata
    [MetadataType(typeof(CourseMetadata))]
    public partial class Cours { }

    public class CharacterMetadata
    {
        [Display(Name = "Name")]
        [StringLength(100, ErrorMessage = "*Less than 100 ")]
        [Required(ErrorMessage = "*Required")]
        public string Name { get; set; }

        [Display(Name = "Alias")]
        [StringLength(50, ErrorMessage = "*Less than 50 ")]
        [DisplayFormat(NullDisplayText = "N/A")]
        public string Alias { get; set; }

        [Display(Name = "Description")]
        [UIHint("MultilineText")]
        [DisplayFormat(NullDisplayText = "N/A")]
        public string Description { get; set; }

        [Display(Name = "Name")]
        [StringLength(100, ErrorMessage = "*Less than 100 ")]
        [DisplayFormat(NullDisplayText = "N/A")]
        public string Occupation { get; set; }

        public bool IsHero { get; set; }
        public bool IsActive { get; set; }
    }//CourseMetadata
    [MetadataType(typeof(CharacterMetadata))]
    public partial class Character { }

}//end namespace
